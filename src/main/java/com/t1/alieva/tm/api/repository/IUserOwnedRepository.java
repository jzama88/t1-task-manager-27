package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.AbstractUserOwnedModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    boolean existsById(
            @Nullable String userId,
            @Nullable String id) throws
            AbstractFieldException;

    @NotNull
    List<M> findAll(
            @NotNull String userId) throws
            AbstractFieldException;

    @NotNull
    List<M> findAll(
            @NotNull String userId,
            @NotNull Comparator<M> comparator) throws
            AbstractFieldException;

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id) throws
            AbstractFieldException;

    @Nullable
    M findOneByIndex(
            @NotNull String userId,
            @NotNull Integer index) throws
            AbstractFieldException;

    int getSize(
            @NotNull String userId) throws
            AbstractFieldException;

    @Nullable
    M removeOneById(
            @Nullable String userId,
            @Nullable String id) throws
            AbstractFieldException;

    @Nullable
    M removeOneByIndex(
            @Nullable String userId,
            @NotNull Integer index) throws
            AbstractFieldException;

    @Nullable
    M add(
            @Nullable final String userId,
            @NotNull M model) throws
            AbstractFieldException;

    @Nullable
    M removeOne(
            @Nullable final String userId,
            @Nullable M model) throws
            AbstractFieldException;

    void removeAll(@NotNull String userId) throws
            AbstractFieldException;
}

