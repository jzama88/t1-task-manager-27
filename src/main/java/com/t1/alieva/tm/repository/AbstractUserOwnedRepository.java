package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IUserOwnedRepository;
import com.t1.alieva.tm.model.AbstractUserOwnedModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {
    @Override
    public void removeAll(@NotNull String userId) {
      /*  final List<M> models = findAll(userId);
        for (final M model : models) {
            removeOne(model);
        }*/
        removeAll(findAll(userId));
    }

    @Override
    public boolean existsById(
            @Nullable String userId,
            @Nullable String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull String userId) {
        return models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    @Nullable
    public M findOneById(
            @Nullable String userId,
            @Nullable String id) {
        if (userId == null || id == null) return null;
        return models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .filter(r -> id.equals(r.getId()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(
            @NotNull String userId,
            @NotNull Integer index) {
        return models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull String userId) {
        return (int) models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .count();
    }

    @Override
    @Nullable
    public M removeOneById(
            @Nullable String userId,
            @Nullable String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    @Nullable
    public M removeOneByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || index == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    @Nullable
    public M add(
            @Nullable String userId,
            @Nullable M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);

    }

    @Override
    @Nullable
    public M removeOne(@Nullable String userId, @Nullable M model) {
        if (userId == null || model == null) return null;
        return removeOneById(userId, model.getId());
    }


}
