package com.t1.alieva.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getName() {
        return "version";
    }

    @Override
    @NotNull
    public String getArgument() {
        return "-v";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }
}
